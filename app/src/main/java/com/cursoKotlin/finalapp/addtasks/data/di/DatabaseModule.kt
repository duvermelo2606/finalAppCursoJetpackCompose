package com.cursoKotlin.finalapp.addtasks.data.di

import android.content.Context
import androidx.room.Room
import com.cursoKotlin.finalapp.addtasks.data.FinalAppDatabase
import com.cursoKotlin.finalapp.addtasks.data.TaskDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    fun provideTaskDao(finalAppDatabase: FinalAppDatabase): TaskDao {
        return finalAppDatabase.taskDao()
    }

    @Provides
    @Singleton
    fun provideFinalAppDatabase(@ApplicationContext appContext: Context): FinalAppDatabase {
        return Room.databaseBuilder(appContext, FinalAppDatabase::class.java, "TaskDatabase")
            .build()
    }
}