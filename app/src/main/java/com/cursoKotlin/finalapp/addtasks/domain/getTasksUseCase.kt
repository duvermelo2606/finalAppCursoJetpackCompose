package com.cursoKotlin.finalapp.addtasks.domain

import com.cursoKotlin.finalapp.addtasks.data.TaskRepository
import com.cursoKotlin.finalapp.addtasks.ui.model.TaskModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class getTasksUseCase @Inject constructor(
    private val taskRepository: TaskRepository
) {
    operator fun invoke(): Flow<List<TaskModel>> = taskRepository.tasks

}