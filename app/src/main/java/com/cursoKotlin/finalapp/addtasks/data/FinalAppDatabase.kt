package com.cursoKotlin.finalapp.addtasks.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [TaskEntity::class], version = 1)
abstract class FinalAppDatabase : RoomDatabase() {
    abstract fun taskDao(): TaskDao
}