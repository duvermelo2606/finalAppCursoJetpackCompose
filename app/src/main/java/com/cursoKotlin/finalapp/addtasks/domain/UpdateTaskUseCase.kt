package com.cursoKotlin.finalapp.addtasks.domain

import com.cursoKotlin.finalapp.addtasks.data.TaskRepository
import com.cursoKotlin.finalapp.addtasks.ui.model.TaskModel
import javax.inject.Inject

class UpdateTaskUseCase @Inject constructor(private val taskRepository: TaskRepository) {
    suspend operator fun invoke(taskModel: TaskModel) {
        taskRepository.update(taskModel)
    }
}