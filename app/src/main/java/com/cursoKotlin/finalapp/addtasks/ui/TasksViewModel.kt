package com.cursoKotlin.finalapp.addtasks.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cursoKotlin.finalapp.addtasks.domain.AddTaskUseCase
import com.cursoKotlin.finalapp.addtasks.domain.UpdateTaskUseCase
import com.cursoKotlin.finalapp.addtasks.domain.deleteTaskUseCase
import com.cursoKotlin.finalapp.addtasks.domain.getTasksUseCase
import com.cursoKotlin.finalapp.addtasks.ui.TasksUiState.Success
import com.cursoKotlin.finalapp.addtasks.ui.model.TaskModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TasksViewModel @Inject constructor(
    private val addTaskUseCase: AddTaskUseCase,
    private val updateTaskUseCase: UpdateTaskUseCase,
    private val deleteTaskUseCase: deleteTaskUseCase,
    getTasksUseCase: getTasksUseCase
): ViewModel() {

    private val _showDialog = MutableLiveData<Boolean>()
    val showDialog: LiveData<Boolean> = _showDialog

//    private val _tasks = mutableStateListOf<TaskModel>()
//    val task: List<TaskModel> = _tasks

    val uiState: StateFlow<TasksUiState> = getTasksUseCase().map( ::Success )
        .catch { TasksUiState.Error(it) }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), TasksUiState.Loading)

    fun onDialogClose() {
        _showDialog.value = false
    }

    fun onTasksCreated(task : String) {
        _showDialog.value = false
        viewModelScope.launch {
            addTaskUseCase(TaskModel(task = task))
        }
    }

    fun onShowDialogClick() {
        _showDialog.value = true
    }

    fun onCheckBoxSelected(taskModel: TaskModel) {
        // Actualizar check de la tarea
//        val index = _tasks.indexOf(taskModel)
//        _tasks[index] = _tasks[index].let {
//            it.copy(selected = !it.selected)
//        }
        viewModelScope.launch {
            updateTaskUseCase(taskModel.copy(selected = !taskModel.selected))
        }
    }

    fun onItemRemove(taskModel: TaskModel) {
        //Borrar tarea
    //      val task = _tasks.find { it.id == taskModel.id }
//        _tasks.remove(task)
        viewModelScope.launch {
            deleteTaskUseCase(taskModel)
        }
    }

}