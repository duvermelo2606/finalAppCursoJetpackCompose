package com.cursoKotlin.finalapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FinalApp:Application()